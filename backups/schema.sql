--
-- PostgreSQL database dump
--

-- Dumped from database version 15.3 (Debian 15.3-1.pgdg120+1)
-- Dumped by pg_dump version 15.3 (Debian 15.3-1.pgdg120+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: get_merge_request_dynamics(bigint, bigint); Type: FUNCTION; Schema: public; Owner: abdt-user
--

CREATE FUNCTION public.get_merge_request_dynamics(_from bigint, _to bigint) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
  declare
    last_range integer = (
      EXTRACT(epoch FROM (
        SELECT AVG(merged_at - created_at) AS avg_duration
        FROM mergerequest_request
        WHERE merged_at IS NOT NULL AND created_at > to_timestamp(_from / 1000)::DATE
      ))
    );
    previous_range integer = (
      EXTRACT(epoch FROM (
        SELECT AVG(merged_at - created_at) AS avg_duration
        FROM mergerequest_request
        WHERE merged_at IS NOT NULL AND created_at > to_timestamp(_from / 1000)::DATE - ((_to - _from) / 86400000)::INTEGER AND created_at < to_timestamp(_from / 1000)::DATE
      ))
    );
	_result varchar(200) = '';
  begin
  	IF last_range > previous_range THEN
      SELECT into _result concat('+', ((last_range * 100 / previous_range) - 100), '%');
    ELSE
	  SELECT into _result concat((100 - (previous_range * 100 / last_range)), '%');
    END IF;
	return _result;
  end
$$;


ALTER FUNCTION public.get_merge_request_dynamics(_from bigint, _to bigint) OWNER TO "abdt-user";

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: commit_commit; Type: TABLE; Schema: public; Owner: abdt-user
--

CREATE TABLE public.commit_commit (
    id character varying(255) NOT NULL,
    message text NOT NULL,
    committed_date timestamp with time zone NOT NULL,
    web_url character varying(255) NOT NULL,
    author_user_id bigint,
    project_id bigint
);


ALTER TABLE public.commit_commit OWNER TO "abdt-user";

--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: abdt-user
--

CREATE TABLE public.django_migrations (
    id bigint NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO "abdt-user";

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: abdt-user
--

ALTER TABLE public.django_migrations ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.django_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: gitlab_group; Type: TABLE; Schema: public; Owner: abdt-user
--

CREATE TABLE public.gitlab_group (
    id bigint NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE public.gitlab_group OWNER TO "abdt-user";

--
-- Name: gitlab_group_id_seq; Type: SEQUENCE; Schema: public; Owner: abdt-user
--

ALTER TABLE public.gitlab_group ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.gitlab_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: gitlab_project; Type: TABLE; Schema: public; Owner: abdt-user
--

CREATE TABLE public.gitlab_project (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    group_id bigint NOT NULL
);


ALTER TABLE public.gitlab_project OWNER TO "abdt-user";

--
-- Name: gitlab_project_id_seq; Type: SEQUENCE; Schema: public; Owner: abdt-user
--

ALTER TABLE public.gitlab_project ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.gitlab_project_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1
);


--
-- Name: gitlab_user; Type: TABLE; Schema: public; Owner: abdt-user
--

CREATE TABLE public.gitlab_user (
    id bigint NOT NULL,
    username character varying(255) NOT NULL,
    name character varying(255),
    email character varying NOT NULL
);


ALTER TABLE public.gitlab_user OWNER TO "abdt-user";

--
-- Name: gitlab_user_id_seq; Type: SEQUENCE; Schema: public; Owner: abdt-user
--

ALTER TABLE public.gitlab_user ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.gitlab_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1
);


--
-- Name: gitlab_user_project; Type: TABLE; Schema: public; Owner: abdt-user
--

CREATE TABLE public.gitlab_user_project (
    user_id bigint NOT NULL,
    project_id bigint NOT NULL
);


ALTER TABLE public.gitlab_user_project OWNER TO "abdt-user";

--
-- Name: mergerequest_note; Type: TABLE; Schema: public; Owner: abdt-user
--

CREATE TABLE public.mergerequest_note (
    id bigint NOT NULL,
    body text NOT NULL,
    request_id bigint NOT NULL,
    reviewer_user_id bigint NOT NULL,
    created_at timestamp with time zone NOT NULL
);


ALTER TABLE public.mergerequest_note OWNER TO "abdt-user";

--
-- Name: mergerequest_note_id_seq; Type: SEQUENCE; Schema: public; Owner: abdt-user
--

ALTER TABLE public.mergerequest_note ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.mergerequest_note_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: mergerequest_request; Type: TABLE; Schema: public; Owner: abdt-user
--

CREATE TABLE public.mergerequest_request (
    id bigint NOT NULL,
    project_id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    merged_at timestamp with time zone,
    changes_count integer,
    source_branch character varying(255) NOT NULL,
    target_branch character varying(255) NOT NULL,
    author_user_id bigint NOT NULL,
    reviewer_user_id bigint
);


ALTER TABLE public.mergerequest_request OWNER TO "abdt-user";

--
-- Name: mergerequest_request_id_seq; Type: SEQUENCE; Schema: public; Owner: abdt-user
--

ALTER TABLE public.mergerequest_request ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.mergerequest_request_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1
);


--
-- Name: commit_commit commit_commit_pkey; Type: CONSTRAINT; Schema: public; Owner: abdt-user
--

ALTER TABLE ONLY public.commit_commit
    ADD CONSTRAINT commit_commit_pkey PRIMARY KEY (id);


--
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: abdt-user
--

ALTER TABLE ONLY public.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: gitlab_group gitlab_group_pkey; Type: CONSTRAINT; Schema: public; Owner: abdt-user
--

ALTER TABLE ONLY public.gitlab_group
    ADD CONSTRAINT gitlab_group_pkey PRIMARY KEY (id);


--
-- Name: gitlab_project gitlab_project_pkey; Type: CONSTRAINT; Schema: public; Owner: abdt-user
--

ALTER TABLE ONLY public.gitlab_project
    ADD CONSTRAINT gitlab_project_pkey PRIMARY KEY (id);


--
-- Name: gitlab_user gitlab_user_pkey; Type: CONSTRAINT; Schema: public; Owner: abdt-user
--

ALTER TABLE ONLY public.gitlab_user
    ADD CONSTRAINT gitlab_user_pkey PRIMARY KEY (id);


--
-- Name: gitlab_user_project gitlab_user_project_pk; Type: CONSTRAINT; Schema: public; Owner: abdt-user
--

ALTER TABLE ONLY public.gitlab_user_project
    ADD CONSTRAINT gitlab_user_project_pk PRIMARY KEY (user_id, project_id);


--
-- Name: gitlab_user gitlab_user_un; Type: CONSTRAINT; Schema: public; Owner: abdt-user
--

ALTER TABLE ONLY public.gitlab_user
    ADD CONSTRAINT gitlab_user_un UNIQUE (email);


--
-- Name: gitlab_user gitlab_user_username_key; Type: CONSTRAINT; Schema: public; Owner: abdt-user
--

ALTER TABLE ONLY public.gitlab_user
    ADD CONSTRAINT gitlab_user_username_key UNIQUE (username);


--
-- Name: mergerequest_note mergerequest_note_pkey; Type: CONSTRAINT; Schema: public; Owner: abdt-user
--

ALTER TABLE ONLY public.mergerequest_note
    ADD CONSTRAINT mergerequest_note_pkey PRIMARY KEY (id);


--
-- Name: mergerequest_request mergerequest_request_pkey; Type: CONSTRAINT; Schema: public; Owner: abdt-user
--

ALTER TABLE ONLY public.mergerequest_request
    ADD CONSTRAINT mergerequest_request_pkey PRIMARY KEY (id);


--
-- Name: commit_commit_author_user_id_d7c6a848; Type: INDEX; Schema: public; Owner: abdt-user
--

CREATE INDEX commit_commit_author_user_id_d7c6a848 ON public.commit_commit USING btree (author_user_id);


--
-- Name: commit_commit_id_5a243624_like; Type: INDEX; Schema: public; Owner: abdt-user
--

CREATE INDEX commit_commit_id_5a243624_like ON public.commit_commit USING btree (id varchar_pattern_ops);


--
-- Name: commit_commit_project_id_94ec6819; Type: INDEX; Schema: public; Owner: abdt-user
--

CREATE INDEX commit_commit_project_id_94ec6819 ON public.commit_commit USING btree (project_id);


--
-- Name: gitlab_project_group_id_31691c8f; Type: INDEX; Schema: public; Owner: abdt-user
--

CREATE INDEX gitlab_project_group_id_31691c8f ON public.gitlab_project USING btree (group_id);


--
-- Name: gitlab_user_email_idx; Type: INDEX; Schema: public; Owner: abdt-user
--

CREATE INDEX gitlab_user_email_idx ON public.gitlab_user USING btree (email);


--
-- Name: gitlab_user_name_52148577_like; Type: INDEX; Schema: public; Owner: abdt-user
--

CREATE INDEX gitlab_user_name_52148577_like ON public.gitlab_user USING btree (name varchar_pattern_ops);


--
-- Name: gitlab_user_project_project_id_a34a089b; Type: INDEX; Schema: public; Owner: abdt-user
--

CREATE INDEX gitlab_user_project_project_id_a34a089b ON public.gitlab_user_project USING btree (project_id);


--
-- Name: gitlab_user_project_user_id_254e0b03; Type: INDEX; Schema: public; Owner: abdt-user
--

CREATE INDEX gitlab_user_project_user_id_254e0b03 ON public.gitlab_user_project USING btree (user_id);


--
-- Name: gitlab_user_username_798f171c_like; Type: INDEX; Schema: public; Owner: abdt-user
--

CREATE INDEX gitlab_user_username_798f171c_like ON public.gitlab_user USING btree (username varchar_pattern_ops);


--
-- Name: mergerequest_note_request_id_cb1117b7; Type: INDEX; Schema: public; Owner: abdt-user
--

CREATE INDEX mergerequest_note_request_id_cb1117b7 ON public.mergerequest_note USING btree (request_id);


--
-- Name: mergerequest_note_reviewer_user_id_52860d9f; Type: INDEX; Schema: public; Owner: abdt-user
--

CREATE INDEX mergerequest_note_reviewer_user_id_52860d9f ON public.mergerequest_note USING btree (reviewer_user_id);


--
-- Name: mergerequest_request_author_user_id_918474fd; Type: INDEX; Schema: public; Owner: abdt-user
--

CREATE INDEX mergerequest_request_author_user_id_918474fd ON public.mergerequest_request USING btree (author_user_id, reviewer_user_id);


--
-- Name: commit_commit commit_commit_author_user_id_d7c6a848_fk_gitlab_user_id; Type: FK CONSTRAINT; Schema: public; Owner: abdt-user
--

ALTER TABLE ONLY public.commit_commit
    ADD CONSTRAINT commit_commit_author_user_id_d7c6a848_fk_gitlab_user_id FOREIGN KEY (author_user_id) REFERENCES public.gitlab_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: commit_commit commit_commit_project_id_94ec6819_fk_gitlab_project_id; Type: FK CONSTRAINT; Schema: public; Owner: abdt-user
--

ALTER TABLE ONLY public.commit_commit
    ADD CONSTRAINT commit_commit_project_id_94ec6819_fk_gitlab_project_id FOREIGN KEY (project_id) REFERENCES public.gitlab_project(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gitlab_project gitlab_project_group_id_31691c8f_fk_gitlab_group_id; Type: FK CONSTRAINT; Schema: public; Owner: abdt-user
--

ALTER TABLE ONLY public.gitlab_project
    ADD CONSTRAINT gitlab_project_group_id_31691c8f_fk_gitlab_group_id FOREIGN KEY (group_id) REFERENCES public.gitlab_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gitlab_user_project gitlab_user_project_project_id_a34a089b_fk_gitlab_project_id; Type: FK CONSTRAINT; Schema: public; Owner: abdt-user
--

ALTER TABLE ONLY public.gitlab_user_project
    ADD CONSTRAINT gitlab_user_project_project_id_a34a089b_fk_gitlab_project_id FOREIGN KEY (project_id) REFERENCES public.gitlab_project(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gitlab_user_project gitlab_user_project_user_id_254e0b03_fk_gitlab_user_id; Type: FK CONSTRAINT; Schema: public; Owner: abdt-user
--

ALTER TABLE ONLY public.gitlab_user_project
    ADD CONSTRAINT gitlab_user_project_user_id_254e0b03_fk_gitlab_user_id FOREIGN KEY (user_id) REFERENCES public.gitlab_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: mergerequest_note mergerequest_note_request_id_cb1117b7_fk_mergerequ; Type: FK CONSTRAINT; Schema: public; Owner: abdt-user
--

ALTER TABLE ONLY public.mergerequest_note
    ADD CONSTRAINT mergerequest_note_request_id_cb1117b7_fk_mergerequ FOREIGN KEY (request_id) REFERENCES public.mergerequest_request(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: mergerequest_note mergerequest_note_reviewer_user_id_52860d9f_fk_gitlab_user_id; Type: FK CONSTRAINT; Schema: public; Owner: abdt-user
--

ALTER TABLE ONLY public.mergerequest_note
    ADD CONSTRAINT mergerequest_note_reviewer_user_id_52860d9f_fk_gitlab_user_id FOREIGN KEY (reviewer_user_id) REFERENCES public.gitlab_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: mergerequest_request mergerequest_request_gitlab_author_user_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: abdt-user
--

ALTER TABLE ONLY public.mergerequest_request
    ADD CONSTRAINT mergerequest_request_gitlab_author_user_id_fk FOREIGN KEY (author_user_id) REFERENCES public.gitlab_user(id);


--
-- Name: mergerequest_request mergerequest_request_gitlab_review_user_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: abdt-user
--

ALTER TABLE ONLY public.mergerequest_request
    ADD CONSTRAINT mergerequest_request_gitlab_review_user_id_fk FOREIGN KEY (reviewer_user_id) REFERENCES public.gitlab_user(id);


--
-- PostgreSQL database dump complete
--