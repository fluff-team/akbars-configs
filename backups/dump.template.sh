export $(grep -v '^#' .env | xargs -0)

postgres_dump() {
    echo "Psql dump process started..."

    current_time=$(date +"%Y-%m-%dT%H:%M:%S%:Z")
    echo "    Current time: $current_time"

    output_file_name="$current_time.dump"
    echo "    Output file: $output_file_name"

    $DOCKER_BIN run --net=host --rm postgres pg_dump -Fc postgres://${POSTGRES_USER}:${POSTGRES_PASSWORD}@${HOSTNAME}:${POSTGRES_PORT_EXTRERNAL}/${POSTGRES_DB} > $output_file_name
    echo "    Dump completed. Uploading into S3..."

    $S3CMD_BIN put $output_file_name s3://abdt-backup/postgres/$output_file_name
    echo "    Uploaded into S3"

    rm $output_file_name
    echo "Local file removed"

    echo "    Psql dump process completed"
    echo
}

nifi_dump() {
    echo "Nifi dump process started..."

    current_time=$(date +"%Y-%m-%dT%H:%M:%S%:Z")
    echo "    Current time: $current_time"

    output_file_name="$current_time.tar.gz"
    echo "    Output file: $output_file_name"

    tar -czf $output_file_name nifi-volume/conf
    echo "    Dump completed"

    echo "    Uploading into S3..."
    $S3CMD_BIN put $output_file_name s3://abdt-backup/nifi/$output_file_name
    echo "    Uploaded into S3"

    rm $output_file_name
    echo "    Local file removed"

    echo "    Nifi dump process completed"
    echo
}

grafana_dump() {
    echo "Grafana dump process started..."

    current_time=$(date +"%Y-%m-%dT%H:%M:%S%:Z")
    echo "    Current time: $current_time"

    output_file_name="$current_time.db"
    echo "    Output file: $output_file_name"

    cp grafana-volume/grafana.db $output_file_name
    echo "    Dump completed. Uploading into S3..."

    $S3CMD_BIN put $output_file_name s3://abdt-backup/grafana/$output_file_name
    echo "    Uploaded into S3"

    rm $output_file_name
    echo "    Local file removed"

    echo "    Grafana dump process completed"
    echo
}

postgres_dump
nifi_dump
grafana_dump
