CREATE OR REPLACE FUNCTION public.get_merge_request_dynamics(
	_from bigint,
	_to bigint)
    RETURNS character varying
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
  declare
    last_range integer = (
      EXTRACT(epoch FROM (
        SELECT AVG(merged_at - created_at) AS avg_duration
        FROM mergerequest_request
        WHERE merged_at IS NOT NULL AND created_at > to_timestamp(_from / 1000)::DATE
      ))
    );
    previous_range integer = (
      EXTRACT(epoch FROM (
        SELECT AVG(merged_at - created_at) AS avg_duration
        FROM mergerequest_request
        WHERE merged_at IS NOT NULL AND created_at > to_timestamp(_from / 1000)::DATE - ((_to - _from) / 86400000)::INTEGER AND created_at < to_timestamp(_from / 1000)::DATE
      ))
    );
	_result varchar(200) = ''; 
  begin
  	IF last_range > previous_range THEN 
      SELECT into _result concat('+', ((last_range * 100 / previous_range) - 100), '%');
    ELSE 
	  SELECT into _result concat((100 - (previous_range * 100 / last_range)), '%');
    END IF;
	return _result;
  end
$BODY$;

ALTER FUNCTION public.get_merge_request_dynamics(bigint, bigint)
    OWNER TO "abdt-user";
