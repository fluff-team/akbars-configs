export $(grep -v '^#' .env | xargs -0)

startup_grafana() {
    echo "Preparing for Grafana startup..."

    if [ ! -d "$GRAFANA_VOLUME" ]
    then
        echo "$GRAFANA_VOLUME does not exist"

        mkdir -p ${GRAFANA_VOLUME}
        cp backups/grafana.db  $GRAFANA_VOLUME/grafana.db
        echo "Default database created"
        
        sqlite3 ${GRAFANA_VOLUME}/grafana.db "update data_source set url = '$POSTGRES_HOST:$POSTGRES_PORT', password = '$POSTGRES_PASSWORD', user = '$POSTGRES_USER' where name = '$GF_DATASOURCE_NAME';"
	    echo "Default credentials updated"

    else
        echo "Grafana volume $GRAFANA_VOLUME already created, skipping"
    fi

    echo
}

startup_nifi() {
    echo "Preparing for Nifi startup..."

    if [ ! -d "$NIFI_VOLUME" ]
    then
        echo "$NIFI_VOLUME does not exist"

        tar -xzf backups/nifi-conf.tar.gz
        
        echo "Default conf created"

    else
        echo "Nifi volume $NIFI_VOLUME already created, skipping"
    fi

    echo
}

wait_nifi() {
    sleep 5
    
    echo "Downloading JDBC for Nifi..."
    docker exec abdt-nifi curl -o /opt/nifi/nifi-current/lib/postgresql-42.6.0.jar https://jdbc.postgresql.org/download/postgresql-42.6.0.jar &> /dev/null
    docker restart abdt-nifi &> /dev/null
    echo "Download complete. Restarting..."
    
    sleep 5
    docker compose logs -f | grep -qe "The UI is available at the following URLs:"
}

startup_docker() {
    docker compose up -d

    echo "Waiting for apps to launch..."

    wait_nifi

    echo
}

post_startup_postgres() {
    docker compose logs -f | grep -qe "listening on IPv6 address"

    echo "Checking database relations..."

    schema_dump=schema.sql
    docker cp backups/$schema_dump abdt-db:/var/lib/postgresql/data/
    docker exec abdt-db psql -U $POSTGRES_USER -d $POSTGRES_DB -f /var/lib/postgresql/data/$schema_dump &> /dev/null
    
    echo "Database schema loaded"

    echo
}

post_startup_crontab() {
    echo "Setting up crontab for periodic dumps..."

    pwd=$(pwd)
    dump_template=backups/dump.template.sh
    crontab_dir=$pwd/crontab-volume
    mkdir -p $crontab_dir

    export DOCKER_BIN=$(which docker)
    export S3CMD_BIN=$(which s3cmd)

    crontab_script=$crontab_dir/dump.sh
    envsubst '$${DOCKER_BIN},$${S3CMD_BIN}' < $dump_template > $crontab_script
    chmod +x $crontab_script

    crontab_tmp=$crontab_dir/crontab-conf
    crontab -l > $crontab_tmp
    echo "0 0 * * * cd $pwd && $crontab_script >> $crontab_dir/dump.log 2>&1" >> $crontab_tmp

    crontab $crontab_tmp

    echo "Crontab ready"
}

startup_grafana
startup_nifi

startup_docker

post_startup_postgres
post_startup_crontab

echo "System launched successfully"
echo "Postgres:"
echo "    Public connection url: postgres://$POSTGRES_USER:$POSTGRES_PASSWORD@$HOSTNAME:$POSTGRES_PORT_EXTRERNAL/$POSTGRES_DB"
echo "    Internal connection url: postgres://$POSTGRES_USER:$POSTGRES_PASSWORD@$HOSTNAME:$POSTGRES_PORT/$POSTGRES_DB"
echo
echo "Grafana: http://${HOSTNAME}:${GF_PORT_EXTRERNAL}"
echo "    Login: admin"
echo "    Password: CHANGEME"
database_uid=$(sqlite3 ${GRAFANA_VOLUME}/grafana.db "select uid from data_source where name = '$GF_DATASOURCE_NAME'")
echo "    Replace password of datasource with '$POSTGRES_PASSWORD' at http://${HOSTNAME}:${GF_PORT_EXTRERNAL}/connections/datasources/edit/${database_uid}"
echo
echo "Nifi: https://${HOSTNAME}:${NIFI_PORT_EXTERNAL}/nifi"
echo "    Login: $SINGLE_USER_CREDENTIALS_USERNAME"
echo "    Password: $SINGLE_USER_CREDENTIALS_PASSWORD"
echo "    Configure JDBC:"
echo "        Url: jdbc:postgresql://abdt-db:$POSTGRES_PORT/$POSTGRES_DB"
echo "        User: $POSTGRES_USER"
echo "        Password: $POSTGRES_PASSWORD"
