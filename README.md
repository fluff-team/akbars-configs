# ABDT Metrics
## This repositry contains
- management scripts
- user documentation

## Requirements:
- unix-like OS (tested macOS Ventura 13.4, Ubuntu 22.04.2 LTS)
- docker & docker compose
- s3cmd
- sqlite3
- envsubst

## Conf files:
- .env file
- ~/.s3cf file
* You should create files manually before deployment. Examples are in System Administrator's Guide. Do not forget to change credentials before deployment in a production environment.

## Run
```
sh scripts/bootstrap.sh
```