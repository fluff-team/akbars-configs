## Руководство по Grafana

Здесь вы можете найти основные вопросы и ответы, касающиеся работы с графиками в Grafana.
Если изложенной здесь информации будет недостаточно, всегда можно обратиться к [официальной документации](https://grafana.com/docs/grafana/latest/). 

<details>
<summary><b>Структура</b></summary>

- `Data Source`: Источник Данных откуда берутся данные для построения графика

- `Dashboard`: Доска где создаются и хранятся *Panles*. Одну *Доску* можно сделать *по-умолчению*(default) что бы она отаброжалась на гланой странице при входе на сайт. Еще можно создавать папки что бы сгруппировать доски

- `Dashboard variables`: Переменные можно создовать внутри Досок. Они помагают фильтовать данные которе будут отаброжаться в панелях

- `Panel`: Панели создаются и хранятся внутри Досок и отабражают данные в разных графиках. А так же они могут превращаться в *Секций*(Collapsable section). В таком виде они могут хранить в себе другие панели и их можно скрывать что бы упростить вид *Доски*

</details>


<details>
<summary><b>Data Source</b></summary>

Data Source находится в странице `Configuration`. Страницу можно найти нажав на кнопку, с иконкой шестерёнки, снизу на левой панели. В секций Data Source можно добавлять новый Истночник Данных или редактировать уже существующие Источники. Grafana поддерживает много разных Database сервисов.

Что бы подлкючить Источник Данных нжны следующие параметры:

- `Host` & `Port`
- `Database name`
- `User` & `Password`

Другие параметры можно настроить в Меню создание

</details>


<details>
<summary><b>Dashboards</b></summary>

Доски находятся в странице `Dashboards`. Страницу можно найти нажав на кнопку, с иконкой 4 квадратика, сверху на левой панели. В секций Dashboards можно добавлять нлвые Доски и Папки или или редактировать уже существующие. 

Еще доски можно отметить звездочками. Отмеченные Доски будут дополнительно храниться в странице `Starred`. Страницу можно найти нажав на кнопку, с иконкой звевздочки, сверху на левой панели.

Одну Доску можно сделать *по-умолчению*(default) что бы она отаброжалась на гланой странице при входе на сайт. Что бы это сделать нужно зайти в секцию `Preferences` на странице `Configurations`. Затем в параметр `Home dashboard` выбрать нужную Доску

</details>


<details>
<summary><b>Dashboard variables</b></summary>

Переменные находятся в Досках и примеяються только к доске в которой находятся. В каждой доске изначально есть перемнная `Дата`. Его можно внутри доски, справа сверху.

Каждая переменная имеет свое мини-меню для конфигураций. У переменнойй `Дата` немного отличается от переменных которые вы можете создать.
В коде к ним можно обращаться вот так: `$__timeFrom()` и `$__timeTo()`


Что бы создать свою пременную, нужно зайти в секцию `Variables` на странице `Dashboard settings`. Страницу можно найти нажав на кнопку, с иконкой шестерёнки, справа на верхней панели.

В секций можно создать новую переменную или редактировать существующие пременные.

Есть 4 вида переменных

- `Query`: Получаются с помощю запраса в вашу датабазау(Датабаза должна быть заранее подключена). Вы можете получить лист значений из которых потом можно выбирать один или несколько для фильтраций. Возможность выбрать несколько можно настроить отдельно.
- `Custom`: Также как *query*, только вместо запроса вы вручную пишите значений
- `Text box`: Эта переменная дает возможность писать свое текстовое значение с доски
- `Constant`: Также как *text box*, только без возможности ввести значение. Только костантное значение

</details>


<details>
<summary><b>Panel</b></summary>

Что бы создать панель в Доске, нужно нажать на кнопку `Add pnale` , с иконкой плюс и график, справа на верхней панели. После нажатия на доске появиться панел меню с 4 опций: 

- **Добватиь новый панел** 
- **Выбрать панель из библиотеки**
- **Добавить новую строку**(Collapsable section)
- **Вставить с буфера**: Для вставки сначала надо скопировать уже существующий панел


<summary><b>Добватиь новый панел</b></summary>
Нажав на эту опцию вы сраху переходите в редактирование этой панели. В целом в этом виде экран поеделен на 3 основных секций. 

- `График`: Слева cверух. Берет данные с запроса и отабражает в графике которую вы выбирает и настраиваете
- `Запрос в Датабазу`: Cлева снизу.
- `Параметры графика`: Спарва

Что бы сделать запрос, сначала надо выбрать Источник данных из существующийх.

Можно использовать конструктор запроса или написать сам код. Переход между ними находиться справа в секций `Запрос в Датабазу`.

Во время запроса можно посмотреть ввиде тадлицы, *switch* для этого находиться посередине сверху секий `График`.

Выбор других видов графика находится на самом вехру в секций `Параметры графика`.
Стоющие упоминание параметры графика:

 - Title
 - Legend
 - Standard options: можно выбрать тип занчений(Int, percent, float, ...)
 - Threshold

</details>



<details>
<summary><b>Примеры гарфиков и запросов</b></summary>


<img src="img/Screenshot 2023-07-15 at 2.10.45 PM.png" alt="4.png" width="500"/>

На доске будут отаброжаться `Lable` название переменных.

Что бы использовать их в коде, нужно использовать `Name` название переменного.
Например: `$committer`
```
WHERE commits.committer_name = ANY(ARRAY[$committer])
```

Даты можно использовать вот так: `$__timeFrom()` и `$__timeTo()`
```
WHERE committed_date >= $__timeFrom() and committed_date <= $__timeTo()
```


<details>
<summary><b>Пример 1</b></summary>

![Alt text](<img/Screenshot 2023-07-15 at 1.40.02 PM.png>)

```
SELECT
    COUNT(*) FILTER ( 
        WHERE EXTRACT( HOUR FROM committed_date ) >= 9 AND EXTRACT( HOUR FROM committed_date ) < 18 
    ) AS В_рабочее_время,
    COUNT(*) FILTER ( 
        WHERE EXTRACT( HOUR FROM committed_date ) < 9 OR EXTRACT( HOUR FROM committed_date ) >= 18 
    ) AS В_не_рабочее_вермя
FROM
  commits;
```
</details>



<details>
<summary><b>Пример 2</b></summary>

![Alt text](<img/Screenshot 2023-07-15 at 1.36.15 PM.png>)

```
SELECT
    DATE_TRUNC('day', committed_date at time zone '-03') AS day,
    COUNT(
        CASE WHEN EXTRACT(HOUR FROM committed_date at time zone '-03' ) BETWEEN 9 AND 17 THEN id END
    ) AS "В рабочее время",
    COUNT( 
        CASE WHEN EXTRACT( HOUR FROM committed_date at time zone '-03') < 9 
        OR EXTRACT( FROM committed_date at time zone '-03' ) >= 18 THEN id END 
    ) AS "Не в рабочее время",
    STRING_AGG(DISTINCT(committer_name), ', ') AS Сотрудники

FROM
    commits
    LEFT JOIN projects ON projects.project_id = commits.project_id

WHERE
    committed_date >= $__timeFrom()
    AND committed_date <= $__timeTo()
    AND commits.committer_name = ANY(ARRAY [$committer])
    AND projects.project_name = ANY(ARRAY [$project])

GROUP BY
    day,
    committer_name

ORDER BY
    day;
```

</details>


<details>
<summary><b>Пример 3</b></summary>

![Alt text](<img/Screenshot 2023-07-15 at 1.57.17 PM.png>)

```
SELECT 
    CONCAT(
        FLOOR(EXTRACT(EPOCH FROM avg_duration) / 86400), 'day ',
        FLOOR(EXTRACT(EPOCH FROM avg_duration) % 86400 / 3600), 'h : ',
        FLOOR(EXTRACT(EPOCH FROM avg_duration) % 3600 / 60), 'm : ',
        FLOOR(EXTRACT(EPOCH FROM avg_duration) % 60), 's'
    ) AS avg_duration_formatted

FROM 
    (
        SELECT AVG(merged_at - created_at) AS avg_duration
        FROM mergerequest_request
        WHERE merged_at IS NOT NULL AND created_at > $__timeFrom()
    ) AS t;
```

</details>




</details>